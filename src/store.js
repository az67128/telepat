import { writable, get } from 'svelte/store';
import { goto } from '$app/navigation';
import dict from './dict';

export const teams = writable([0, 0]);
export const questions = writable(dict.sort(() => (Math.random() > 0.5 ? 1 : -1)));
export const activeTeam = writable(0);

export const addScore = (value) => {
	teams.update((state) => {
		const i = get(activeTeam);
		state[i] += value;
		return state;
	});
};

export const passTurn = () => {
	questions.update((state) => {
		return state.slice(1);
	});
	activeTeam.update((state) => {
		return state === 0 ? 1 : 0;
	});
	const [team1, team2] = get(teams);
	goto(team1 > 10 || team2 > 10 ? '/endgame' : '/startturn', { replaceState: true });
};

export const newGame = () => {
	teams.set([0, 0]);
	activeTeam.set(0);
	goto('/startturn', { replaceState: true });
};
