export default function drag(node) {
	let y;
	let boundingClientRect = node.getBoundingClientRect();

	const calculateAndDispatch = (e, type) => {
		const event = e.touches ? e.touches[0] : e;
		const topCoord = boundingClientRect.y;
		const maxY = boundingClientRect.height;
		y = event.clientY - topCoord;
		if (y < 0) y = 0;
		if (y > maxY) y = maxY;
		node.dispatchEvent(
			new CustomEvent(type, {
				detail: { y }
			})
		);
	};
	function handleMousedown(e) {
		boundingClientRect = node.getBoundingClientRect();
		calculateAndDispatch(e, 'dragstart');

		window.addEventListener('mousemove', handleMousemove);
		window.addEventListener('touchmove', handleMousemove);
		window.addEventListener('mouseup', handleMouseup);
		window.addEventListener('touchend', handleMouseup);
	}

	function handleMousemove(e) {
		calculateAndDispatch(e, 'dragmove');
	}

	function handleMouseup() {
		// calculateAndDispatch(e, 'dragend');

		window.removeEventListener('mousemove', handleMousemove);
		window.removeEventListener('touchmove', handleMousemove);
		window.removeEventListener('mouseup', handleMouseup);
		window.removeEventListener('touchend', handleMouseup);
	}

	node.addEventListener('mousedown', handleMousedown);
	node.addEventListener('touchstart', handleMousedown);

	return {
		destroy() {
			node.removeEventListener('mousedown', handleMousedown);
			node.removeEventListener('touchstart', handleMousedown);
		}
	};
}
